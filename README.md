# Restaurant Critique Generator

This project will generate a random point of critique towards a fast food restaurant.
It will complain about any of the following categories:

- Unsanitary environment
- Rude staff
- Incompetent staff
- Unpleasant guests
- Insufficient/incorrect information
- Long waits
- Administrative errors

## Examples
- The bathrooms at this place.. Don't go there.
- I never even got my food, I think they screwed me
- A veteran in front of me in the line was screaming about why they didn't have soups.
- I asked about where the straws were and they said I was stupid for not finding.
- Some old geezer in front of me in the line caused a big fuzz over why they didn't have a buffet.
- Asked for unsalted fries, they said they couldn't do that.
- There was a gang of annoying idiots that tossed salt packets everywhere.