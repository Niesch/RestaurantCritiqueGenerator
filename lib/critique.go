package RestaurantCritiqueGenerator

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"os"
	"strings"
	"time"
)

type CritiqueGenerator struct {
	StringSets []StringSet
}

type StringSet struct {
	Type        string
	BaseStrings []string
	Replacers   []Replacer
}

type Replacer struct {
	From     string
	ToEither []string
}

func (c *CritiqueGenerator) GetIncompetentStaffLine() string {
	f, err := os.Open("critique.json")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	d := json.NewDecoder(f)
	err = d.Decode(c)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	f.Close()

	rand.Seed(time.Now().UTC().UnixNano())
	// Pick a random StringSet
	ss := c.StringSets[rand.Intn(len(c.StringSets))]
	// Pick a random BaseString from that StringSet
	bs := ss.BaseStrings[rand.Intn(len(ss.BaseStrings))]
	// Apply each replacer on it with a certain chance
	for _, rep := range ss.Replacers {
		if rand.Intn(5) <= 3 {
			bs = strings.NewReplacer(rep.From, rep.ToEither[rand.Intn(len(rep.ToEither))]).Replace(bs)
		}
	}
	return bs

}
